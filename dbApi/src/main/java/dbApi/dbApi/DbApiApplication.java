package dbApi.dbApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication
public class DbApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbApiApplication.class, args);
	}


}
