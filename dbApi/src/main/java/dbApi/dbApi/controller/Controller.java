package dbApi.dbApi.controller;

import dbApi.dbApi.model.Client;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class Controller {

    @RequestMapping(value = "merge", method = RequestMethod.GET)
    @ResponseBody
    public String getEvents() {
        return "merge";

    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    @ResponseBody
    public String ceva(@RequestParam(value = "code") String code){
        System.out.println(code);
        return code;
    }

    @RequestMapping(path = "/oauth2-client-webapp/{token}", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public String base64(@PathVariable("token") String token){
        Client client = new Client();
        client.setToken(token);

        return client.getToken();
    }

    @RequestMapping(path = "/oauth2-client-webapp/redirect", method = RequestMethod.GET)
    @ResponseBody
    public String token(){
        return "token";
    }

    @RequestMapping(path = "/error", method = RequestMethod.GET)
    public String eroare(){
        return "EROARE";
    }


}
