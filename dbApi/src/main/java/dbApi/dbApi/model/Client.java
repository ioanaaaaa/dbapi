package dbApi.dbApi.model;

public class Client {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Client{" +
                "token='" + token + '\'' +
                '}';
    }
}
