import {Component} from '@angular/core';
import {UserAuth} from '../user-auth';
import {LoggedInService} from './../services/logged-in.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  private loginService: any[];
  private userAuth: UserAuth;
  myDate: String = null;

  constructor(public logged: LoggedInService) {
    logged.isLoggedIn = false;
    console.log("ceva: " + logged);
    localStorage.removeItem('expires_At');
  }

}
