import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CashAccount} from "../cash-account";
import {UserAuth} from "../user-auth";
import {Person} from "../person";
import {LoggedInService} from './../services/logged-in.service'
import {AlertService} from 'ngx-alerts';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-access-token',
  templateUrl: './access-token.component.html',
  styleUrls: ['./access-token.component.css']
})
export class AccessTokenComponent implements OnInit {

  cashAccount:CashAccount[];
  person: Person;
  user: UserAuth;
  elements: Element[];
  element: Element;
  token:string;
  copyToken: String;
  isTriggered: boolean = false;
  now: Date = new Date();
  myDate: String = null;
  initialHour: Date;
  id = 'chart1';
  width = 600;
  height = 400;
  type = 'pie3d';
  dataFormat = 'json';
  dataSource;

  constructor(private _cookieService: CookieService, private alertService: AlertService, private route: ActivatedRoute, public userToken: UserAuth, public logged: LoggedInService) {

    route.params.subscribe(val => {

      console.log(this.token);
      logged.isLoggedIn = true;
      console.log(logged.isLoggedIn);
      console.log("Called constructor from TOKEN-ACCESS");
      var url = window.location.href.indexOf('access_token').valueOf();
      var nr2 = window.location.href.indexOf('token_type') - 1;

      if (this.userToken.token == null) {
        this.token = window.location.href.substring(49, nr2);
        this._cookieService.put('token', this.token);
        console.log('token null');
        console.log(this.token);

      }
      else {
        console.log('token nenull');
        this.userToken.token = this._cookieService.get('token');
        console.log(this.userToken.token);
      }


      if (this.userToken.token == null) {
       this.userToken.token = this.token;

      }
      localStorage.setItem('token', this.token);
      // this.userToken.token=localStorage.getItem('token');

      console.log("accounttoken");
      console.log(this.userToken.token);

      console.log("token");
      console.log(this.token);


      console.log("Set Test Cookie as Test");


    });
  }

  ngOnInit() {
    this.getChart();
  }

  functie() {
    console.log("FUNCTIE");
  }

  alertEndTime() {
    if (this.isTriggered == false) {
      this.alertService.danger('Your session has expired!');
      this.isTriggered = true;
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_at");
    const expiresAt = JSON.parse(expiration);
    return expiresAt;
  }

  logout() {
    localStorage.removeItem('expires_At');
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

  getChart() {
    this.dataSource = {
      "chart": {
        "caption": "Sales Per Employee for 2014",
        "palette": "2",
        "animation": 1,
        "animationDuration": 1,
        "formatnumberscale": "1",
        "decimals": "0",
        "numberprefix": "$",
        "pieslicedepth": "30",
        "startingangle": "125",
        "showborder": "0"
      },
      "data": [
        {
          "label": "Leverling",
          "value": "100524",
          "issliced": "1"
        },
        {
          "label": "Fuller",
          "value": "87790",
          "issliced": "1"
        },
        {
          "label": "Davolio",
          "value": "81898",
          "issliced": "0"
        },
        {
          "label": "Peacock",
          "value": "76438",
          "issliced": "0"
        },
        {
          "label": "King",
          "value": "57430",
          "issliced": "0"
        },
        {
          "label": "Callahan",
          "value": "55091",
          "issliced": "0"
        },
        {
          "label": "Dodsworth",
          "value": "43962",
          "issliced": "0"
        },
        {
          "label": "Suyama",
          "value": "22474",
          "issliced": "0"
        },
        {
          "label": "Buchanan",
          "value": "21637",
          "issliced": "0"
        }
      ]
    }
  }

}
