import {Component, OnInit} from '@angular/core';
import {AccessTokenService} from '../services/access-token.service';
import {UserAuth} from '../user-auth';
import {Transaction} from '../transaction';
import {CashAccount} from "../cash-account";
import {CookieService} from 'angular2-cookie/core';
import {LoggedInService} from '../services/logged-in.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  step = 0;
  nr=0;

  setStep(index: number) {
    this.step = index;
    this.step = index;
  }

  nextStep() {
    this.step++;
    this.nr++;
  }

  prevStep() {
    this.step--;
    this.nr--;
  }

  transactions: Transaction[];
  transaction: Transaction;
  cashAccount: CashAccount;
  cashAccounts: CashAccount[];
  amounts: number[];
  amount: number;

  constructor(private _cookieService: CookieService, public accessService: AccessTokenService, public user: UserAuth, public logged: LoggedInService) {
    logged.isLoggedIn = true;
  }

  ngOnInit() {
    // this.user.token=localStorage.getItem('token');

    this.user.token = this._cookieService.get('token');
    this.cashAccounts = [];
    this.transactions=[];
    this.accessService.cashAccounts(this.user.token).subscribe((info:any) => {
      for(let infoo of info){
        this.cashAccount=infoo;
        this.cashAccounts.push(this.cashAccount);
        console.log(this.cashAccount.iban);


        this.accessService.getTransactions(this.user.token, this.cashAccount.iban).subscribe((info:any) => {
          for (let infoo of info){
            this.transaction=infoo;
            this.transactions.push(this.transaction);
            console.log(this.transaction);
          }

          console.log(this.transactions);
        });
      }
    })
  }
}
