import {Injectable} from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  {state: 'access-token1', name: 'Starter Page', type: 'link', icon: 'av_timer'},
  {state: 'personal-information', type: 'link', name: 'Personal informations', icon: 'view_comfy'},
  {state: 'cash-accounts', type: 'link', name: 'Cash Accounts', icon: 'view_comfy'},
  {state: 'transactions', type: 'link', name: 'Transactions', icon: 'view_comfy'},
  {state: 'balance-graph', type: 'link', name: 'Balance', icon: 'view_comfy'}
];

@Injectable()

export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }

}
