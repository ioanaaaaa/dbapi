export class Address {
    street:string;
    houseNumber:string;
    zip:string;
    city:string;
    country:string;
    registeredResidence:boolean;
    addressType:string;
}
export class Addresses{
    addresses:Address[];
}
