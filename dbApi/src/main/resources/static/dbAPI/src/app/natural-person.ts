export class NaturalPerson {
  firstName:string
  lastName:string
  dateOfBirth:string
  gender:string
  titleOfNobility:string
  nationality:string
  birthName:string
  birthPlace:string
}
