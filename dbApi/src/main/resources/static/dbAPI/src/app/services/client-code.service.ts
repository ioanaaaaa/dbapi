import {Injectable} from '@angular/core';
import {Headers, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {HttpClient} from "@angular/common/http";
import {Buffer} from "buffer";


@Injectable()
export class ClientCodeService {
  // de modificat cu clientID si secretKEY al vostru
  clientId:String = "79873db5-cee5-48fb-82a7-b8496714062d";
  clientSecret:String = "AKw73NlKi-Qro4oCyhK6ODquCSyOErKvF6dDZuDgaq6NBibNPAJPvtDDL6rKuZjqJ7lld2IP7bdxc38EExsy5NE";
  encodedData = new Buffer(this.clientId + ':' + this.clientSecret).toString('base64');


  private baseUrl: string = 'https://simulator-api.db.com/gw/oidc/token?';
  private headers = new Headers({'Access-Control-Allow-Headers': "origin, authorization, accept",
    'Accept': 'application/json',
    'Access-Control-Allow-Methods':'GET, POST, PUT',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Origin': 'https://simulator-api.db.com',
    "Authorization": "Basic " + this.encodedData,
    'Content-Type': 'application/x-www-form-urlencoded'});
  private options = new RequestOptions({headers: this.headers});


  constructor(private _http: HttpClient) {

  }

  getToken(grant:String) {
    console.log(this.encodedData);
    console.log(this.baseUrl + grant);
    return this._http.post(this.baseUrl+grant , this.options).map((response: Response) => response.json())
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {
    return Observable.throw(error || 'Server error');
  }


}
