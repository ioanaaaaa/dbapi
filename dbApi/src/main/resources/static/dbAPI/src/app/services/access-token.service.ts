import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DateFormatPipe} from '../date-format-pipe';

@Injectable()
export class AccessTokenService {

  private baseUrl: string = 'https://simulator-api.db.com/gw/dbapi';
  // cashAccount: CashAccount[];

  constructor(private _http: HttpClient, private datePipe: DateFormatPipe) {
  }

  cashAccounts(token: String) {
    var _headers = new HttpHeaders({"Authorization": "Bearer " + token});

    return this._http.get(this.baseUrl + "/v1/cashAccounts", {headers: _headers})
      .map((response: Response) => response)
      .catch(this.errorHandler);
  }


  getClientName(token: String) {
    var _headers = new HttpHeaders({"Authorization": "Bearer " + token});

    return this._http.get(this.baseUrl + "/v1/partners", {headers: _headers})
      .map((response: Response) => response)
      .catch(this.errorHandler);
  }

  getTransactions(token: string, iban:String) {
    var _headers = new HttpHeaders({"Authorization": "Bearer " + token});

    return this._http.get(this.baseUrl + "/v1/transactions?iban=" + iban, {headers: _headers})
      .map((response: Response) => response)
      .catch(this.errorHandler);
  }

  getAddresses(token: string) {
    var _headers = new HttpHeaders({"Authorization": "Bearer " + token});

    return this._http.get(this.baseUrl + "/v2/addresses", {headers: _headers})
      .map((response: Response) => response)
      .catch(this.errorHandler);
  }

  errorHandler(error: Response) {
    return Observable.throw(error || 'Server error');
  }

  transformDate(date) {
    this.datePipe.transform(date);
  }
}
