import {Component, OnInit} from '@angular/core';
import {LoggedInService} from '../services/logged-in.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(public logged: LoggedInService) {
    logged.isLoggedIn = true;
  }

  ngOnInit() {
  }

}
