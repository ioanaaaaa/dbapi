export class ChartItem {
  label: string;
  value: number;
  show: number;

  constructor(label: string, value: number, show: number) {
    this.label = label;
    this.value = value;
    this.show = show;
  }
}
