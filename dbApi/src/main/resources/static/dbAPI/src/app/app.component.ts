import {Component} from '@angular/core';
import {MenuItems} from './menu-items'
import {LoggedInService} from './services/logged-in.service';
import {AlertService} from 'ngx-alerts';
import {AccessTokenService} from './services/access-token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  showFiller = false;
  isTriggered: boolean = false;
  now: Date = new Date();
  myDate: String;
  date: any;

  constructor(private accessService: AccessTokenService, private alertService: AlertService, public menuItems: MenuItems, public logged: LoggedInService) {
    logged.isLoggedIn = false;
    console.log(logged.isLoggedIn);
    this.getTimerFromURL();
  }

  getTimerFromURL() {
    if(localStorage.getItem('expires_At'))
    {
      this.date = localStorage.getItem('expires_At');
      this.myDate = this.date.toString();
      console.log("Before init: "+this.myDate);
    }
    else if (this.date == null && this.myDate == null) {
      console.log("BEFORE: " + this.now.getHours() + ":" + this.now.getMinutes() + ":" + this.now.getSeconds());
      this.now.setSeconds(this.now.getSeconds() + 599);
      this.accessService.transformDate(this.now.setMilliseconds(this.now.getMilliseconds() + 599));
      console.log("AFTER: " + this.now.getHours() + ":" + this.now.getMinutes() + ":" + this.now.getSeconds());
      this.myDate = this.now.toString();
      localStorage.setItem('expires_At',this.myDate.toString());
    }
  }

  alertEndTime() {
    if (this.isTriggered == false) {
      this.alertService.danger('Your session has expired!');
      this.isTriggered = true;
      this.date = localStorage.getItem('expires_At');
      this.myDate = this.date.toString();
    }
  }

  logout() {
    localStorage.removeItem('expires_At');
    console.log("logout");
  }

}
