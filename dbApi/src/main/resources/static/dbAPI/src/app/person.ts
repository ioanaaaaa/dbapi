import {NaturalPerson} from "./natural-person";

export class Person {
  partnerType: string;
  naturalPerson: NaturalPerson;
  community: Community;
  organization: Organization;
  emailAddresses: EmailAddress[];
  phoneNumbers: PhoneNumber[];
}

export class EmailAddress {
  emailAddressType: string;
  emailAddress: string;
}

export class PhoneNumber {
  communicationType: string;
  internationalAreaCode: string;
  areaCode: string;
  telephoneNumber: string
}

export class Organization {

}

export class Community {

}
