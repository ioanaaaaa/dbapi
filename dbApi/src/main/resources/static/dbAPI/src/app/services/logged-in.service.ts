import {Injectable} from '@angular/core';

@Injectable()
export class LoggedInService {

  isLoggedIn: boolean;
  numberOfRefresh: number;

  constructor() {
    this.isLoggedIn = false;
    this.numberOfRefresh = 1;
  }

}
