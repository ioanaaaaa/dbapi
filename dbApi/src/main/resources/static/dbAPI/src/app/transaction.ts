export class Transaction {
  originIban: string;
  amount: number;
  counterPartyName: string;
  counterPartyIban: string;
  paymentReference: string;
  bookingDate: string;
}
