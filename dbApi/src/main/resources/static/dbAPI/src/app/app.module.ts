import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';

import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ClientCodeService} from "./services/client-code.service";
import {HttpClientModule} from "@angular/common/http";
import {AccessTokenComponent} from './access-token/access-token.component';
import {AccessTokenService} from './services/access-token.service';
import {HomePageComponent} from './home-page/home-page.component';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';

import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MenuItems} from './menu-items';
import {CashAccountsComponent} from './cash-accounts/cash-accounts.component';
import {PersonalInformationComponent} from './personal-information/personal-information.component';
import {UserAuth} from './user-auth';
import {LoggedInService} from './services/logged-in.service';
import {CountdownTimerModule} from 'ngx-countdown-timer';
import {AlertModule} from 'ngx-alerts';
import {DateFormatPipe} from './date-format-pipe';
import {MomentModule} from 'ngx-moment';
import {TransactionsComponent} from './transactions/transactions.component';
import {CookieService} from 'angular2-cookie/services/cookies.service';
import {BalanceGraphComponent} from './balance-graph/balance-graph.component';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import {FusionChartsModule} from 'angular4-fusioncharts';
import {GlobalVarService} from './../app/services/global-var.service';

const appRoutes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'access-token1', component: AccessTokenComponent},
  {path: 'personal-information', component: PersonalInformationComponent},
  {path: 'cash-accounts', component: CashAccountsComponent},
  {path: 'transactions', component: TransactionsComponent},
  {path: 'balance-graph', component: BalanceGraphComponent}
]

FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccessTokenComponent,
    HomePageComponent,
    CashAccountsComponent,
    PersonalInformationComponent,
    TransactionsComponent,
    BalanceGraphComponent,
  ],
  exports:[
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MatListModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSidenavModule,
    MatListModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    CountdownTimerModule.forRoot(),
    AlertModule.forRoot({maxMessages: 1, timeout: 3000}),
    // JwtModule.forRoot({
    //   config: {
    //     headerName: 'Authorization',
    //     authScheme: 'Bearer ',
    //     skipWhenExpired: true,
    //     tokenGetter: () => {
    //       return localStorage.getItem('access_token');
    //     },
    //     whitelistedDomains: ['localhost:4200'],
    //     blacklistedRoutes: ['localhost:4200/access-token/']
    //   }
    // }),
    MomentModule,
    FusionChartsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [ClientCodeService, CookieService, HttpClientModule, AccessTokenService, MenuItems, LoggedInService, DateFormatPipe, AccessTokenComponent, UserAuth, GlobalVarService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
