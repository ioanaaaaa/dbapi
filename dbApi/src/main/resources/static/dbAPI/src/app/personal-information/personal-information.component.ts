import {Component, OnInit} from '@angular/core';
import {UserAuth} from "../user-auth";
import {AccessTokenService} from "../services/access-token.service";
import {EmailAddress, Person, PhoneNumber} from "../person";
import {Address, Addresses} from "../address";
import {LoggedInService} from '../services/logged-in.service';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent implements OnInit {


  person: Person;
  addresses: Addresses;
  address: Address[];
  emails: EmailAddress[];
  phoneNumbers: PhoneNumber[];
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  constructor(private _cookieService: CookieService, public user: UserAuth, public accessService: AccessTokenService, public logged: LoggedInService) {
    logged.isLoggedIn = true;
  }

  ngOnInit() {
    // this.user.token=localStorage.getItem('token');

    this.user.token = this._cookieService.get('token');

    this.accessService.getClientName(this.user.token).subscribe((info:any) => {
      this.person=info;
      this.emails = this.person.emailAddresses;
      this.phoneNumbers = this.person.phoneNumbers;
      console.log(this.emails);
      console.log(this.person);
    });
    this.accessService.getAddresses(this.user.token).subscribe((data: any) => {
      console.log(data);
      this.addresses = data;
      this.address = this.addresses.addresses;
      for (let address of this.addresses.addresses) {
        console.log("address");
        console.log(address);
      }
      console.log(this.addresses);

    });
  }

}
