import {Component, OnInit} from '@angular/core';
import {CashAccount} from "../cash-account";
import {AccessTokenService} from '../services/access-token.service';
import {Person} from "../person";
import {NaturalPerson} from '../natural-person';
import {UserAuth} from '../user-auth';
import {CookieService} from 'angular2-cookie/core';
import {LoggedInService} from '../services/logged-in.service';
import {MatDatepickerInputEvent} from '@angular/material';
import {GlobalVarService} from '../services/global-var.service';

@Component({
  selector: 'app-cash-accounts',
  templateUrl: './cash-accounts.component.html',
  styleUrls: ['./cash-accounts.component.css']
})
export class CashAccountsComponent implements OnInit {
  token: String;
  cashAccounts: CashAccount[];
  cashAccount: CashAccount;
  person: Person;
  naturalPerson: NaturalPerson;
  events: string[] = [];

  constructor(private _cookieService: CookieService, public accessService: AccessTokenService, public user: UserAuth, public logged: LoggedInService, private globalVar: GlobalVarService) {
    logged.isLoggedIn = true;
  }

  getIBAN(iban: any) {
    console.log(iban);
    this.globalVar.currentIBAN = iban;
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${type}: ${event.value}`);
    this.accessService.transformDate(event.value);
    console.log("VAL: " + event.value);

    var dateEvent = event.value;
    // this.specifDay = dateEvent.getDate();
    // this.specifMonth = dateEvent.getMonth() + 1;
    // this.specifYear = dateEvent.getFullYear();
    // this.getChartForSpecificPeriod(this.specifDay, this.specifMonth, this.specifYear);
  }

  ngOnInit() {
    // this.user.token=localStorage.getItem('token');
    this.user.token = this._cookieService.get('token');
    this.cashAccounts = [];
    console.log("cash token");
    console.log(this.user.token);
    this.accessService.cashAccounts(this.user.token).subscribe((info:any) => {
      for(let infoo of info){
        this.cashAccount=infoo;
        this.cashAccounts.push(this.cashAccount);
        console.log(this.cashAccount.iban);
      }

      console.log(this.cashAccounts);

    });

    this.accessService.getClientName(this.user.token).subscribe((info:any) => {
      this.person=info;
      console.log(this.person.naturalPerson);

    })
  }

}
