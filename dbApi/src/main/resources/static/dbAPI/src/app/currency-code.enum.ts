export enum CurrencyCode {
  EUR = "€",
  USD = "$",
  GBP = "£"
}
