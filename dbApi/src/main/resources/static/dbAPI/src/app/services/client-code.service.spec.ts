import { TestBed, inject } from '@angular/core/testing';

import { ClientCodeService } from './client-code.service';

describe('ClientCodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientCodeService]
    });
  });

  it('should be created', inject([ClientCodeService], (service: ClientCodeService) => {
    expect(service).toBeTruthy();
  }));
});
