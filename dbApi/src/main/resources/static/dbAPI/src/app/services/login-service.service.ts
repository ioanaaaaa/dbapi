import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class LoginServiceService {

  private baseUrl: string = 'https://simulator-api.db.com/gw/oidc/authorize';
  private headers = new Headers({'Content-Type': 'application/json'});
  private options = new RequestOptions({headers: this.headers});

  constructor(private _http: Http) {
  }

  // redirectToSecondStep(){
  //   return this._http.get(this.baseUrl, this.options).catch(this.errorHandler);
  // }

  // errorHandler(error:Response){
  //   return Observable.throw(error ||'Server error');
  // }
}
