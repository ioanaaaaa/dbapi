import {Component, OnInit} from '@angular/core';
import {LoggedInService} from '../services/logged-in.service';
import {AccessTokenService} from '../services/access-token.service';
import {UserAuth} from '../user-auth';
import {CashAccount} from '../cash-account';
import {Transaction} from '../transaction';
import {ChartItem} from '../chart-item';
import {CurrencyCode} from '../currency-code.enum';
import {CookieService} from 'angular2-cookie/core';
import {MatDatepickerInputEvent} from '@angular/material';
import {GlobalVarService} from '../services/global-var.service';

@Component({
  selector: 'app-balance-graph',
  templateUrl: './balance-graph.component.html',
  styleUrls: ['./balance-graph.component.css']
})
export class BalanceGraphComponent implements OnInit {

  cashAccounts: CashAccount[];
  cashAccount: CashAccount;
  transactions: Transaction[];
  transaction: Transaction;
  currentBalance: number = 0;
  beginningBalance: number = 0;
  specifiedBalance: number = 0;
  myAmounts: any[];
  myItem: ChartItem;
  currencyCode: any;
  id = 'chart1';
  width = 600;
  height = 400;
  type = 'line';
  dataFormat = 'json';
  dataSource;
  selectedOptionIBAN: any;
  specifDay: any;
  specifMonth: any;
  specifYear: any;

  events: string[] = [];

  constructor(private _cookieService: CookieService, private accessService: AccessTokenService, public user: UserAuth, public logged: LoggedInService, private globalVar: GlobalVarService) {
    logged.isLoggedIn = true;
    this.user.token = this._cookieService.get('token');

    this.getAccounts();
    // this.getChartForSpecificPeriod(20, 4, 2018);
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${type}: ${event.value}`);
    this.accessService.transformDate(event.value);
    console.log("VAL: " + event.value);

    var dateEvent = event.value;
    this.specifDay = dateEvent.getDate();
    this.specifMonth = dateEvent.getMonth() + 1;
    this.specifYear = dateEvent.getFullYear();
    this.valueChanged();
    // this.getChartForSpecificPeriod(this.specifDay, this.specifMonth, this.specifYear);
  }

  ngOnInit() {
  }

  valueChanged() {
    this.selectedOptionIBAN = this.globalVar.currentIBAN;
    this.getCurrentBalanceForOneIBAN(this.selectedOptionIBAN);
    this.getChartForSpecificPeriod(this.specifDay, this.specifMonth, this.specifYear);
  }

  getAccounts() {
    this.cashAccounts = [];
    this.transactions = [];

    this.accessService.cashAccounts(this.user.token).subscribe((info: any) => {
      for (let infoo of info) {
        this.cashAccount = infoo;
        this.cashAccounts.push(this.cashAccount);
        console.log(this.cashAccount.iban);
        this.currentBalance = this.cashAccounts[0].currentBalance;
        this.selectedOptionIBAN = this.cashAccounts[0].iban; // default value
      }
    });

  }

  getCurrentBalanceForOneIBAN(iban: any) {
    this.accessService.cashAccounts(this.user.token).subscribe((info: any) => {
      for (let infoo of info) {
        this.cashAccount = infoo;
        if (infoo.iban == iban)
          this.currentBalance = this.cashAccount.currentBalance;
      }
    });
  }

  getChartForSpecificPeriod(specifiedDay: number, specifiedMonth: number, specifiedYear: number) {

    // this.cashAccounts = [];
    // this.transactions = [];

    // this.accessService.cashAccounts(this.user.token).subscribe((info: any) => {
    //   for (let infoo of info) {
    //     this.cashAccount = infoo;
    //     this.cashAccounts.push(this.cashAccount);
    //     console.log(this.cashAccount.iban);
    //     this.currentBalance = this.cashAccount.currentBalance;
    //     // this.selectedOptionIBAN = this.cashAccounts[0].iban; // default value

        switch (this.cashAccount.currencyCode) {
          case "EUR":
            this.currencyCode = CurrencyCode.EUR;
            break;
          case "GBP":
            this.currencyCode = CurrencyCode.GBP;
            break;
          case "USD":
            this.currencyCode = CurrencyCode.USD;
            break;
          default:
            this.currencyCode = "";
        }
        this.myAmounts = [];

    this.accessService.getTransactions(this.user.token, this.selectedOptionIBAN).subscribe((info: any) => {

          info.sort((a, b) => {
            if (a.bookingDate > b.bookingDate) return -1;
            else if (a.bookingDate < b.bookingDate) return 1;
            else return 0;
          });
      // console.log("desc sorted");
      // console.log(info);

      // set Date.now()
      var datenow: Date = new Date();
      // console.log(this.accessService.transformDate(datenow));
      var monthnow = (datenow.getMonth() + 1) < 10 ? "0" + (datenow.getMonth() + 1) : (datenow.getMonth() + 1);
      this.myItem = new ChartItem(datenow.getFullYear() + "-" + monthnow + "-" + datenow.getDate(), this.currentBalance, 0);
      this.myAmounts.push(this.myItem);

      // set info with first day of transaction => not initialised at first iteration
          var date = info[0].bookingDate;
          var sumOfDay = 0;
      var number;
      var mydate;
      // console.log("specified: " + specifiedDay + " " + specifiedMonth + " " + specifiedYear);

      this.currentBalance -= info[0].amount;
      this.myItem = new ChartItem(date, Math.round(this.currentBalance * 100) / 100, info[0].amount);
      this.myAmounts.push(this.myItem);

          for (let infoo of info) {

            var currentDate = infoo.bookingDate;
            var year = currentDate.substring(0, 4); //console.log(year+" "+parseInt(year));
            var month = currentDate.substring(5, 7); //console.log(month+" "+parseInt(month));
            var day = currentDate.substring(8, 10); //console.log(day+" "+parseInt(day));

            if (((parseInt(day) >= specifiedDay && parseInt(month) == specifiedMonth && parseInt(year) == specifiedYear) || (parseInt(month) > specifiedMonth && parseInt(year) >= specifiedYear))) {

              if (currentDate != date) {
                sumOfDay = 0;
                for (let infoo1 of info) {

                  var date1 = infoo1.bookingDate;
                  var year1 = date1.substring(0, 4); //console.log(year+" "+parseInt(year));
                  var month1 = date1.substring(5, 7); //console.log(month+" "+parseInt(month));
                  var day1 = date1.substring(8, 10); //console.log(day+" "+parseInt(day));

                  if (parseInt(day1) == parseInt(day) && parseInt(month1) == parseInt(month) && parseInt(year1) == parseInt(year)) {
                    sumOfDay += infoo1.amount;
                    number++;
                    date = date1;
                  }
                }
                this.currentBalance -= sumOfDay;
                this.myItem = new ChartItem(date, Math.round(this.currentBalance * 100) / 100, sumOfDay);
                this.myAmounts.push(this.myItem);
              }
            }
          }

          this.myAmounts.sort((a, b) => {
            if (a.label < b.label) return -1;
            else if (a.label > b.label) return 1;
            else return 0;
          });

      // console.log("Length: " + this.myAmounts.length);
      // console.log("myamounts: ");
      // console.log(this.myAmounts);

      this.dataSource = {
        "chart": {
          "caption": "Balance of " + this.selectedOptionIBAN + " account",
          "subCaption": "From " + (this.specifDay < 10 ? "0" + this.specifDay : this.specifDay) + "-" + (this.specifMonth < 10 ? "0" + this.specifMonth : this.specifMonth) + "-" + this.specifYear + " to " + datenow.getFullYear() + "-" + monthnow + "-" + datenow.getDate(),
          "numberprefix": this.currencyCode,
          "theme": "fint",
          "xAxisName": "Day",
          "yAxisName": "Amount",
          "setAdaptiveYMin": "1",
          "setAdaptiveYMax": "1",
          "animation": 1,
          "animationDuration": 1,
          "divlineAlpha": "100",
          "divlineThickness": "1",
          "divLineIsDashed": "1",
          "divLineDashLen": "1",
          "divLineGapLen": "1",
          "lineThickness": "3",
          "flatScrollBars": "1",
          "scrollheight": "10",
          "numVisiblePlot": "12",
          "showHoverEffect": "1",
//           "trendValueFont" : "Arial",
// "trendValueFontSize" : "12",
// "trendValueFontBold" : true,
// "trendValueFontItalic" : true,
// "trendValueBgColor" : "green",
// "trendValueBorderColor" : "black",
// "trendValueAlpha" : "1",
// "trendValueBgAlpha" : "1",
// "trendValueBorderAlpha" : "1",
// "trendValueBorderPadding" : "1",
// "trendValueBorderRadius" : "20",
// "trendValueBorderThickness" : "5",
// "trendValueBorderDashed" :true,
// "trendValueBorderDashLen" : "12",
// "trendValueBorderDashGap" : "12"
        },
        "data": this.myAmounts,
        // "trendlines": [
        //   {
        //     "line": [
        //       {
        //         "startvalue": "3600",
        //         "color": "#1aaf5d",
        //         "valueOnRight": "1",
        //         "displayvalue": "Monthly Target"
        //       }
        //     ]
        //   }
        // ]
      }
        });
  }

}
